<?php

function getSettingsPayUR()
{
    $payment_gateway_id = 'payur';
    $payment_gateways   = WC_Payment_Gateways::instance();
    $payment_array = $payment_gateways->payment_gateways();
    if(!array_key_exists($payment_gateway_id,$payment_array))
        return null;
    $atts = $payment_array[$payment_gateway_id];
    if($atts == null) return null;
    $settings = $atts->settings;
    return $settings;
}

/**
 * Limitar numero de productos en el carrito
 */
function PayUR_limitarNumeroDeProductos( $cartItemData ) {
	wc_empty_cart();

	return $cartItemData;
}
add_filter( 'woocommerce_add_cart_item_data', 'PayUR_limitarNumeroDeProductos', 10, 1 );

function testing()
{
    echo "Testing";
    $api = new PayUR_api(getSettingsPayUR());
    $plan = $api->get_cliente();
    echo "<pre>";
    var_dump($plan);
    echo "</pre>";
}
add_shortcode('testing', 'testing');