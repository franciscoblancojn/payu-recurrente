<?php
/*
 * This action hook registers our PHP class as a WooCommerce payment gateway
 */
add_filter( 'woocommerce_payment_gateways', 'PayUR_add_gateway_class' );
function PayUR_add_gateway_class( $gateways ) {
	$gateways[] = 'WC_PayUR_Gateway'; // your class name is here
	return $gateways;
}
 
/*
 * The class itself, please note that it is inside plugins_loaded action hook
 */
add_action( 'plugins_loaded', 'PayUR_init_gateway_class' );
function PayUR_init_gateway_class() {
 
	class WC_PayUR_Gateway extends WC_Payment_Gateway {
 
 		/**
 		 * Class constructor, more about it in Step 3
 		 */
 		public function __construct() {
            $this->id = 'payur'; // payment gateway plugin ID
            $this->icon = plugin_dir_url( __FILE__ ).'../img/payu-logo.svg'; // URL of the icon that will be displayed on checkout page near your gateway name
            $this->has_fields = true; // in case you need a custom credit card form
            $this->method_title = 'PayUR Gateway';
            $this->method_description = 'Description of PayUR payment gateway'; // will be displayed on the options page
        
            // gateways can support subscriptions, refunds, saved payment methods,
            // but in this tutorial we begin with simple payments
            $this->supports = array(
                'products'
            );
        
            // Method with all the options fields
            $this->init_form_fields();
        
            // Load the settings.
            $this->init_settings();
            $this->title = "PayU Recurrent";
            $this->description = "Pagos Recurrentes con PayU por Tarjeta de Credito";
            //$this->enabled = $this->get_option( 'enabled' );

            // This action hook saves the settings
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
        
 		}
 
		/**
 		 * Plugin options, we deal with it in Step 3 too
 		 */
 		public function init_form_fields(){
            
            $this->form_fields = array(
                'merchant_id' => array(
                    'title'       => 'Merchant ID',
                    'type'        => 'text',
                    'description' => 'Merchant ID of PayU',
                ),
                'API_KEY' => array(
                    'title'       => 'API KEY',
                    'type'        => 'text',
                    'description' => 'API KEY of PayU',
                ),
                'API_LOGIN' => array(
                    'title'       => 'API LOGIN',
                    'type'        => 'text',
                    'description' => 'API LOGIN of PayU',
                ),
                'Llave_publica' => array(
                    'title'       => 'Llave pública',
                    'type'        => 'text',
                    'description' => 'Llave pública of PayU',
                ),
                'maxPaymentsAllowed' => array(
                    'title'       => 'MaxPaymentsAllowed',
                    'type'        => 'number',
                    'description' => 'Maximo de cobros',
                    'default'     => 12
                ),
                'APE' => array(
                    'title'       => 'Pagos Extranjeros',
                    'type'        => 'checkbox',
                    'description' => 'Posee Autorizacion de pagos Extrangeros',
                ),
                'tasa' => array(
                    'title'       => 'Tasa de Cambio',
                    'type'        => 'number',
                    'description' => 'Tasa de Cambio de su moneda actual a PesosColombianos',
                    'default'     => 1
                ),
                'immediatePayment' => array(
                    'title'       => 'Pagos Imediatos',
                    'type'        => 'checkbox',
                    'description' => 'Desea que el cobro de haga al momento de la compra',
                    'default'     => 'yes'  
                ),
            );
 
	 	}
 
 
		/*
		 * Custom CSS and JS, in most cases required only when you decided to go with a custom credit card form
		 */
	 	public function payment_scripts() {
 
            
 
	 	}
 
		
        /*
        * Fields validation, more in Step 5
        */
        public function validate_fields() {
            $name           = isset($_POST[$this->id . '_name']) ?      wc_clean($_POST[$this->id . '_name'])       : '';
            $document       = isset($_POST[$this->id . '_document']) ?  wc_clean($_POST[$this->id . '_document'])   : '';
            $number         = isset($_POST[$this->id . '_number']) ?    wc_clean($_POST[$this->id . '_number'])     : '';
            $cvv            = isset($_POST[$this->id . '_cvv']) ?    wc_clean($_POST[$this->id . '_cvv'])     : '';
            $expMonth       = isset($_POST[$this->id . '_expMonth']) ?  wc_clean($_POST[$this->id . '_expMonth'])   : '';
            $expYear        = isset($_POST[$this->id . '_expYear']) ?   wc_clean($_POST[$this->id . '_expYear'])    : '';
            $type           = isset($_POST[$this->id . '_type']) ?      wc_clean($_POST[$this->id . '_type'])       : '';
            
            $sw = true;
            //name
            if (empty($name)) {
                wc_add_notice('Invalid Name', 'error');
                $sw = false;
            }
            //document
            if (empty($document)) {
                wc_add_notice('Invalid Document', 'error');
                $sw = false;
            }
            //number
            if (empty($number)) {
                wc_add_notice('Credit Card Invalid.', 'error');
                $sw = false;
            }elseif(!preg_match('/\d/', $number)) {
                wc_add_notice('Credit Card Invalid.', 'error');
                $sw = false;
            }
            //expMonth
            if (empty($expMonth)) {
                wc_add_notice('ExpMonth Invalid.', 'error');
                $sw = false;
            } elseif (!preg_match('/^\d{2}$/', $expMonth)) {
                wc_add_notice('ExpMonth Invalid.', 'error');
                $sw = false;
            }
            //expYear
            if (empty($expYear)) {
                wc_add_notice('ExpYear Invalid.', 'error');
                $sw = false;
            } elseif (!preg_match('/^\d{4}$/', $expYear)) {
                wc_add_notice('ExpYear Invalid.', 'error');
                $sw = false;
            }
            //type
            if (empty($type)) {
                wc_add_notice('Type Invalid.', 'error');
                $sw = false;
            }
            return $sw;
        }
 
        /**
         * You will need it if you want your custom credit card form, Step 4 is about it
         */
        public function payment_fields() {
            ob_start();
            ?>
            <fieldset id="<?=$this->id?>-cc-form" class="row_form">
                <p class="form-row form-row-wide">
                    <label for="<?=$this->id?>_type">
                        <?=__("Type", $this->id)?>
                        <span class="required">*</span>
                    </label>
                    <select
                        id="<?=$this->id?>_type" 
                        name="<?=$this->id?>_type" 
                        class="input-text" 
                        type="text" 
                        autocomplete="off"
                        >
                        <?php
                        $list = explode(", ","VISA, AMEX, DINERS, MASTERCARD, DISCOVER, ELO, SHOPPING, NARANJA, CABAL, CREDENCIAL, PRESTO, RIPLEY, CENCOSUD, HIPERCARD, ARGENCARD");
                        for ($i=0; $i < count($list); $i++) { 
                            ?>
                            <option value="<?=$list[$i]?>"><?=$list[$i]?></option>
                            <?php
                        }
                        ?>
                    </select>
                </p>
                <p class="form-row form-row-wide">
                    <label for="<?=$this->id?>_name">
                        <?=__("Name", $this->id)?>
                        <span class="required">*</span>
                    </label>
                    <input
                        id="<?=$this->id?>_name" 
                        name="<?=$this->id?>_name" 
                        class="input-text" 
                        type="text" 
                        autocomplete="off"
                    />
                </p>
                <p class="form-row form-row-wide">
                    <label for="<?=$this->id?>_document">
                        <?=__("Document", $this->id)?>
                        <span class="required">*</span>
                    </label>
                    <input
                        id="<?=$this->id?>_document" 
                        name="<?=$this->id?>_document" 
                        class="input-text" 
                        type="text" 
                        autocomplete="off"
                    />
                </p>
                <p class="form-row form-row-wide">
                    <label for="<?=$this->id?>_number">
                        <?=__("Credit Card", $this->id)?>
                        <span class="required">*</span>
                    </label>
                    <input onblur="this.value = this.value.replace(/[^0-9]/g, '');" 
                        id="<?=$this->id?>_number" 
                        name="<?=$this->id?>_number" 
                        class="input-text" 
                        type="text" 
                        maxlength="20" 
                        autocomplete="off"
                    />
                </p>
                <p class="form-row form-row-wide max-50">
                    <label for="<?=$this->id?>_expMonth">
                        <?=__("Exp Month", $this->id)?>
                        <span class="required">*</span>
                    </label>
                    <select
                        id="<?=$this->id?>_expMonth" 
                        name="<?=$this->id?>_expMonth" 
                        class="input-text" 
                    >
                        <?php
                            for ($i=1 ; $i <= 12; $i++) {
                                $value = (($i<10)?"0":"").$i;
                                ?>
                                <option value="<?=$value?>"><?=$value?></option>
                                <?php
                            }
                        ?>
                    </select>
                </p>
                <p class="form-row form-row-wide max-50">
                    <label for="<?=$this->id?>_expYear">
                        <?=__("Exp Year", $this->id)?>
                        <span class="required">*</span>
                    </label>
                    <select
                        id="<?=$this->id?>_expYear" 
                        name="<?=$this->id?>_expYear" 
                        class="input-text" 
                    >
                        <?php
                            for ($i=date('Y') ; $i <= date('Y') + 50; $i++) {
                                $value = $i;
                                ?>
                                <option value="<?=$value?>"><?=$value?></option>
                                <?php
                            }
                        ?>
                    </select>
                </p>
                <div class="clear"></div>
            </fieldset>
            <style>
                .form-row{
                    width:100%;
                    padding: 0 10px;
                }
                .max-50{
                    max-width:50%;
                }
                .row_form{
                    display:flex;
                    flex-wrap:wrap;
                }
                .woocommerce #payment .form-row select,
                .woocommerce-page #payment .form-row select{
                    width:100%;
                }
            </style>
            <?php
            echo ob_get_clean();
        }
		/*
		 * We're processing the payments here, everything about it is in Step 5
		 */
		public function process_payment( $order_id ) {
            $user_id = get_current_user_id();
            $order = wc_get_order( $order_id );
            foreach ( $order->get_items() as $item_id => $item ) {
                $product_id = $item->get_product_id();
            }

            $_subscription_trial_length = get_post_meta( $product_id , '_subscription_trial_length' , true);
            if($_subscription_trial_length == null || $_subscription_trial_length == ""){
                $_subscription_trial_length = 0;
            }

            $json = '{
                "name": "'.     $_POST[$this->id . '_name'].'",
                "document": "'. $_POST[$this->id . '_document'].'",
                "number": "'.   $_POST[$this->id . '_number'].'",
                "expMonth": "'. $_POST[$this->id . '_expMonth'].'",
                "expYear": "'.  $_POST[$this->id . '_expYear'].'",
                "type": "'.     $_POST[$this->id . '_type'].'"
            }';

            $api = new PayUR_api($this->settings);
            $cliente = $api->get_cliente($user_id);
            if(isset($cliente->type) && $cliente->type == "BAD_REQUEST"){
                $error = '
                    <strong>Cliente '.$cliente->type.'</strong>
                    <p>'.$cliente->description.'</p>
                ';
                if(isset($cliente->errorList)){
                    for ($i=0; $i < count($cliente->errorList); $i++) { 
                       $error .= '
                        <p>'.$cliente->errorList[$i].'</p>
                       '; 
                    }
                }
                wc_add_notice( $error , 'error');
                return;
            }

            $card = $api->create_tarjeta($json);
            if(isset($card->type) && $card->type == "BAD_REQUEST"){
                $error = '
                    <strong>Tarjeta '.$card->type.'</strong>
                    <p>'.$card->description.'</p>
                ';
                if(isset($card->errorList)){
                    for ($i=0; $i < count($card->errorList); $i++) { 
                       $error .= '
                        <p>'.$card->errorList[$i].'</p>
                       '; 
                    }
                }
                wc_add_notice( $error , 'error');
                return;
            }

            $plan = $api->get_plan($product_id);
            if(!isset($plan->id)){
                // <p>'.json_encode($plan).'</p>
                $error = '
                    <strong>Plan '.$plan->type.'</strong>
                    <p>'.$plan->description.'</p>
                ';
                if(isset($plan->errorList)){
                    for ($i=0; $i < count($plan->errorList); $i++) { 
                       $error .= '
                        <p>'.$plan->errorList[$i].'</p>
                       '; 
                    }
                }
                wc_add_notice($error , 'error');
                return;
            }
            //in api erro
            $suscripccion = '{
                "quantity": "12",
                "installments": "1",
                "trialDays": "'.$_subscription_trial_length.'",
                '.(($this->settings["immediatePayment"]=="yes")?'"immediatePayment": true,':'').'
                "customer": {
                   "id": "'.$cliente->id.'",
                   "creditCards": [
                        {
                            "token": "'.$card->token.'"
                        }
                    ]
                },
                "plan": {
                    "planCode": "'.$plan->planCode.'"
                }
            }';

            $r = $api->create_suscripcion($suscripccion);
            if(!isset($r->id)){
                $error = '
                    <strong>Suscripcion'.$r->type.'</strong>
                    <p>'.$r->description.'</p>
                ';
                if(isset($r->errorList)){
                    for ($i=0; $i < count($r->errorList); $i++) { 
                       $error .= '
                        <p>'.$r->errorList[$i].'</p>
                       '; 
                    }
                }
                wc_add_notice($error , 'error');
            }else{
                $sus =  get_user_meta($user_id , 'suscripccion_PayU' , true);
                if($sus == null || $sus == ""){
                    $sus = array();
                }
                $sus[] = $r;
                update_user_meta($user_id , 'suscripccion_PayU' , $sus);
                
                update_post_meta($order_id , 'Suscripcion' , $suscripccion);
                update_post_meta($order_id , 'R' , json_encode($r));
                
                $order->update_status('wc-processing');
                WC()->cart->empty_cart();
                return [
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order)
                ];
            }
	 	}
 
 	}
}