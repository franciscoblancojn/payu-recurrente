<?php
require_once(preg_replace('/wp-content.*$/','',__DIR__).'wp-load.php');

$data = json_decode(file_get_contents('php://input'), true);
if(isset($data)){
    $_POST = $data;
}
function deleteSuscripcion_PayUR($data)
{
    $id = $data["id"];
    $user_id = $data["user_id"];
    $api = new PayUR_api(getSettingsPayUR());
    $sus = $api->delete_suscripcion($id);
    echo json_encode($sus);
    $suscripciones = get_user_meta($user_id , 'suscripccion_PayU' , true);
    for ($i=0; $i < count($suscripciones) ; $i++) { 
        if($suscripciones[$i]->id == $id){
            unset($suscripciones[$i]);
        }
    }
    $suscripciones = array_values($suscripciones);
    update_user_meta($user_id , 'suscripccion_PayU' , $suscripciones );
}
switch ($_POST["action"]) {
    case 'deleteSuscripcion':
        deleteSuscripcion_PayUR($_POST["data"]);
        break;
    
    default:
        echo "Sin resultados";
        break;
}