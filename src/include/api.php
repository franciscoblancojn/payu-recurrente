<?php

class PayUR_api {
    private $url_dev    = "https://sandbox.api.payulatam.com/payments-api/";
    private $url_pro    = "https://api.payulatam.com/payments-api/";
    private $url        = "";
    private $settings   = array();
    /**
     * __construct
     * 
     */
    public function __construct($settings = array())
    {
        $this->url = $this->url_pro;
        $this->settings = $settings;
        if(isset($settings['sandox']) && $settings['sandox'] == "yes"){
            $this->url = $this->url_dev;
        }
        $this->settings['currency'] = get_woocommerce_currency();
        if(isset($settings['APE']) && $settings['APE'] == "yes"){
            $this->settings['tasa'] = 1;
        }else{
            $this->settings['currency'] = "COP";
        }
    }
    /**
     * getAuthorization
     */
    function getAuthorization()
    {
        $Authorization = $this->settings['API_LOGIN'].":".$this->settings['API_KEY'];
        $Authorization = base64_encode($Authorization);
        return $Authorization;
    }
    /**
     * request
     *  encargado de hacer las peticiones a PayU
     */
    public function request($json , $url , $method = "POST")
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json; charset=utf-8",
                "Accept: application/json",
                "Authorization: Basic ".$this->getAuthorization()
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response);
        return ($response);
    }
    /**
     * create_plan
     * encargado de crear plan en PayU
     */
    function create_plan( $product_id )
    {
        $product = wc_get_product( $product_id );
        $_subscription_trial_length = get_post_meta( $product_id , '_subscription_trial_length' , true);
        $json = '{
            "accountId": "'.$this->settings['merchant_id'].'",
            "planCode": "'.$product->get_slug().'",
            "description": "'.$product->get_name().'",
            "interval": "MONTH",
            "intervalCount": "1",
            "trialDays":"'.$_subscription_trial_length.'",
            "maxPaymentsAllowed": "'.$this->settings['maxPaymentsAllowed'].'",
            "paymentAttemptsDelay": "1",
            "additionalValues": [
               {
                  "name": "PLAN_VALUE",
                  "value": "'.($product->get_price() * $this->settings['tasa']).'",
                  "currency": "'.$this->settings['currency'].'"
               },
               {
                  "name": "PLAN_TAX",
                  "value": "0",
                  "currency": "'.$this->settings['currency'].'"
               },
               {
                  "name": "PLAN_TAX_RETURN_BASE",
                  "value": "0",
                  "currency": "'.$this->settings['currency'].'"
               }
            ]
        }';
        return $this->request($json , $this->url."rest/v4.9/plans");
    }
    /**
     * get_plan
     * Obtiene el plan, si no existe lo crea y lo retorna
     */
    public function get_plan( $product_id )
    {
        $product = wc_get_product( $product_id );
        $r = $this->request("" , $this->url."rest/v4.9/plans/".$product->get_slug() , "GET");
        if(isset($r->type) && $r->type == "NOT_FOUND"){
            $r = $this->create_plan( $product_id );
        }
        return $r;
    }
    /**
     * update_plan
     * Actualiza el plan
     */
    public function update_plan( $product_id )
    {
        $product = wc_get_product( $product_id );
        $json = '{
            "planCode": "'.$product->get_slug().'",
            "description": "'.$product->get_name().'",
            "paymentAttemptsDelay": "1",
            "maxPendingPayments": "1",
            "maxPaymentAttempts": "1",
            "additionalValues": [
               {
                  "name": "PLAN_VALUE",
                  "value": "'.($product->get_price() * $this->settings['tasa']).'",
                  "currency": "'.$this->settings['currency'].'"
               },
               {
                  "name": "PLAN_TAX",
                  "value": "0",
                  "currency": "'.$this->settings['currency'].'"
               },
               {
                  "name": "PLAN_TAX_RETURN_BASE",
                  "value": "0",
                  "currency": "'.$this->settings['currency'].'"
               }
            ]
        }';
        echo $json;
        var_dump($this->settings);
        return $this->request($json , $this->url."rest/v4.9/plans/".$product->get_slug() , "PUT");
    }
    /**
     * create_cliente
     * crea el cliente
     */
    function create_cliente( $user_id )
    {
        $user = get_user_by( 'id', $user_id );
        $json = '{
            "fullName": "'.$user->data->user_nicename.'",
            "email": "'.$user->data->user_email.'"
        }';
        $r = $this->request($json , $this->url."rest/v4.9/customers/");
        update_user_meta( $user_id , "customersPayU", $r);

        return $r;
    }
    /**
     * get_cliente
     * Obtiene el cliente, si no existe lo crea 
     */
    public function get_cliente( $user_id = null)
    {
        if($user_id == null){
            $user_id = get_current_user_id();
        }
        if($user_id == null || $user_id == ""){
            return json_decode('{
                "type" : "BAD_REQUEST"
                "description" : "user no found"
            }');
        }
        $customersPayU = get_user_meta( $user_id , "customersPayU", true);
        if($customersPayU == null || $customersPayU == ""){
            $customersPayU = $this->create_cliente( $user_id );
        }
        return $this->request("" , $this->url."rest/v4.9/customers/".$customersPayU->id , "GET");
    }
    /**
     * update_cliente
     * Actualiza el cliente, si no existe lo crea
     */
    public function update_cliente( $user_id )
    {
        $customersPayU = get_user_meta( $user_id , "customersPayU", true);
        if($customersPayU == null || $customersPayU == ""){
            return $this->get_cliente( $user_id );
        }

        $user = get_user_by( 'id', $user_id );
        $json = '{
            "fullName": "'.$user->data->user_nicename.'",
            "email": "'.$user->data->user_email.'"
        }';
        return $this->request( $json , $this->url."rest/v4.9/customers/".$customersPayU->id , "PUT");
    }
    /**
     * create_tarjeta
     * crea la tarjeta en PayU
     */
    public function create_tarjeta( $json )
    {
        $user_id    = get_current_user_id();
        $customer   = $this->get_cliente($user_id);

        //$json = json_encode($data);

        return $this->request( $json , $this->url."rest/v4.9/customers/".$customer->id."/creditCards");
    }
    /**
     * get_tarjeta
     * obtiene la tarjeta en PayU
     */
    public function get_tarjeta( $creditCardId )
    {
        return $this->request( "" , $this->url."rest/v4.9/creditCards/".$creditCardId , "GET" );
    }
    /**
     * create_suscripccion
     * crea la suscripcion con el json de gateway
     */
    public function create_suscripcion($json)
    {
        return $this->request( $json , $this->url."rest/v4.9/subscriptions/" );
    }
    /**
     * get_suscripcion
     * obtiene la suscripcion por medio del id
     */
    public function get_suscripcion($id)
    {
        return $this->request( "" , $this->url."rest/v4.9/recurringBill?susbcriptionId=".$id,"GET" );
    }
    /**
     * delete_suscripcion
     * elimina la suscripcion por medio del id
     */
    public function delete_suscripcion($id)
    {
        //return [$this->url."rest/v4.9/subscriptions/".$id];
        return $this->request( "" , $this->url."rest/v4.9/subscriptions/".$id ,"DELETE");
    }
}