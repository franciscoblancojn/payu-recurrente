<?php
/**
 * Require gateway
 * gateway for payU
 */
require_once plugin_dir_path( __FILE__ )."functions.php";
require_once plugin_dir_path( __FILE__ )."api.php";
require_once plugin_dir_path( __FILE__ )."gateway.php";
require_once plugin_dir_path( __FILE__ )."end_point.php";