<?php

add_action('init', function() {
	add_rewrite_endpoint('suscripciones', EP_ROOT | EP_PAGES);
});
add_filter('woocommerce_account_menu_items', function($items) {
	$items['suscripciones'] = __('Suscripciones PayU');
	return $items;
});
add_action('woocommerce_account_suscripciones_endpoint', function() {
    $user_id = get_current_user_id();
    ?>
    <div>
        <h3><?=__("Suscripciones PayU")?></h3>
        <?=show_suscripcion_PayU()?>
    </div>
    <style>
        pre{
            text-align: left;
            text-transform: none;
        }
    </style>
    <script>
        deleteSuscripcion = (id) =>{
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            var raw = JSON.stringify({
                "action":"deleteSuscripcion",
                "data":{
                    "id": id,
                    "user_id": '<?=get_current_user_id()?>'
                }
            });

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };

            fetch("<?=plugin_dir_url( __FILE__ )."actions.php";?>", requestOptions)
            .then(response => response.text())
            .then(result =>{
                result = JSON.parse(result)
                console.log(result)
                //alert(result.description)
                if(window.location.search.indexOf("test")==-1){
                    window.location.reload()
                }
            })
            .catch(error => console.log('error', error));
        }
    </script>
    <?php
});
function show_suscripcion_PayU()
{
    $user_id = get_current_user_id();
    $sus  = get_user_meta($user_id , 'suscripccion_PayU' , true);
    if($sus == null || $sus == ""){
        return '<p>
            '.__("No posees suscripciones").'
        </p>';
    }
    ob_start();
    ?>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Interval</th>
                <th>Value</th>
                <th>Estado</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?=array_reduce($sus , "reduce_suscripccion_PayU")?>
        </tbody>
    </table>
    <?php
    return ob_get_clean();
}
function reduce_suscripccion_PayU($carry, $item)
{
    $values = $item->plan->additionalValues;
    $value = 0;
    for ($i=0; $i < count($values); $i++) { 
        if($values[$i]->name == "PLAN_VALUE"){
            $value = wc_price($values[$i]->value);
        }
    }
    $api = new PayUR_api(getSettingsPayUR());
    ob_start();
    ?>
    <tr>
        <td><?=$item->plan->planCode?></td>
        <td><?=$item->plan->description?></td>
        <td><?=$item->plan->interval?></td>
        <td><?=$value?></td>
        <td>
            <?php
            $sus = $api->get_suscripcion($item->id);
            echo getSubscriptionId_PayUR($sus->recurringBillList , $item->id);
            ?>
        </td>
        <td>
            <button onclick="deleteSuscripcion('<?=$item->id;?>')">
                Delete
            </button>
        </td>
    </tr>
    <?php
    return $carry . ob_get_clean();
}
function getSubscriptionId_PayUR($recurringBillList , $id)
{
    ob_start();
    for ($i=0; $i < count($recurringBillList) ; $i++) { 
        if($recurringBillList[$i]->subscriptionId == $id){
            echo $recurringBillList[$i]->state;
            echo "<pre>";
            var_dump($recurringBillList[$i]);
            echo "</pre>";
        }
    }
    return ob_get_clean();
}